class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remaining_guesses = 6
  end

  def play
    setup

    while @remaining_guesses > 0
      p board
      take_turn

      if board.all?
        p board
        puts "Guesser wins! The word was #{referee.secret_word}."
        return
      end
    end

    puts "Guesser loses! The word was #{referee.secret_word}."
  end

  def setup
    secret_word_length = referee.pick_secret_word
    guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def take_turn
    guess = guesser.guess(board)
    indices = referee.check_guess(guess)
    update_board(guess, indices)
    @remaining_guesses -= 1 if indices.empty?

    guesser.handle_response(guess, indices)
  end

  def update_board(guess, indices)
    indices.each { |i| @board[i] = guess }
  end
end

class HumanPlayer
  FULL_DICTIONARY = File.readlines("lib/dictionary.txt").map(&:chomp)

  attr_reader :secret_word

  def secret_word_prompt
    print "What is the secret word? "
    @secret_word = gets.chomp

    until FULL_DICTIONARY.include?(@secret_word.downcase)
      puts "Input is not a word in the dictionary."
      secret_word_prompt
    end
  end

  def pick_secret_word
    secret_word_prompt

    @secret_word.length
  end

  def check_guess(guess)
    puts "Player guessed #{guess}."
    print "Which indicies is that letter located? "
    gets.chomp.split(",").map(&:to_i)
  end

  def register_secret_length(length)
    puts "Secret word is #{length} letters long."
  end

  def guess(board)
    print "What is your guess? "
    gets.chomp
  end

  def handle_response(guess, indices)
    if indices.empty?
      puts "#{guess} is not in secret word!"
    else
      puts "Found #{guess} at indices #{indices}."
    end
  end
end

class ComputerPlayer
  FULL_DICTIONARY = File.readlines("lib/dictionary.txt").map(&:chomp)

  attr_reader :dictionary, :candidate_words, :secret_word

  def initialize(dictionary=FULL_DICTIONARY)
    @dictionary = dictionary
    @secret_word = dictionary.sample
  end

  def pick_secret_word
    @secret_word.length
  end

  def check_guess(guess)
    (0..secret_word.length).select { |i| guess == secret_word[i] }
  end

  def register_secret_length(secret_length)
    @candidate_words = dictionary.select do
      |word| word.length == secret_length
    end
  end

  def guess(board)
    letters_hash = Hash.new(0)

    @candidate_words.each do |word|
      word.chars.each_with_index do |char, i|
        letters_hash[char] += 1 if board[i].nil?
      end
    end

    letter, count = letters_hash.sort_by { |letter, count| count }.last
    letter
  end

  def handle_response(guess, indices)
    @candidate_words.select! do |word|
      exact_matches = indices.all? { |i| word[i] == guess }

      exact_matches && word.count(guess) == indices.size
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  print "Referee: Computer (y/n)? "
  if gets.chomp == "y"
    referee = ComputerPlayer.new
  else
    referee = HumanPlayer.new
  end

  print "Guesser: Computer (y/n)? "
  if gets.chomp == "y"
    guesser = ComputerPlayer.new
  else
    guesser = HumanPlayer.new
  end
  
  Hangman.new({guesser: guesser, referee: referee}).play
end
